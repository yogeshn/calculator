//Author: Abhineet Swain
import { Component } from '@angular/core';

@Component({
    selector: 'js-calculator',
    templateUrl: './app/calculator/calculator.component.html',
    host: {
        '(document:keypress)': 'handleKeyboardEvents($event)'
    }
})

export class CalculatorComponent {

    lcdValue: string = '';
    lcdValueExpression: boolean = false;
    clearValue: string = 'AC'
    completePattern = new RegExp("[0-9-+*/.()pi]");
    numbersPattern = new RegExp('^[0-9]+$');
    lcdBackGroundColor: string = '#424242';
    parenthesisFlag: number = 0;
    expression: string = '=';
    history: string = '';

    constructor() { }

    // Manages all calculator buttons except "AC", "Reset", & "Clear".
    calcButtonPress(inputValue: string) {
        if (this.lcdValue === 'Error' || this.lcdValue === 'NaN') this.lcdValue = '';

        if (this.validateInput(inputValue)) {
          if('/*-+'.indexOf(inputValue) === -1 && !this.lcdValueExpression) {

          } else if(this.completePattern.test(inputValue)) {
              this.lcdValue += inputValue;
          } else if(inputValue === 'pi') {
              this.lcdValue += inputValue;
          } else if(inputValue === 'sin') {
              this.lcdValue += inputValue;
          } else if(inputValue === 'cos') {
              this.lcdValue += inputValue;
          } else if(inputValue === 'tan') {
              this.lcdValue += inputValue;
          } else if(inputValue === 'log') {
              this.lcdValue += inputValue;
          }
          else if(inputValue === '=') {
              this.evaluate();
              inputValue = "";
              this.lcdValue += inputValue;
              return;
          }
          this.flipACButton(true);
        } else {
             this.flashLCD();
       }
    }

    random(){
        var math: any = require('node_modules/mathjs/dist/math.js');
        this.lcdValue = math.round(math.random(1,1000), 2);
    }

    /*****************************************************************
     Little Typescript/Javascript function to RESET TextBox.
     Also refreshed the Page too.
    *****************************************************************/
    reset(){
       this.lcdValue = '';
       this.expression = '';
       location.reload();
    }

    // Function for handling keyboard inputs.
    // Route keyboard input to their appropriate functions.
    handleKeyboardEvents(keyPress: KeyboardEvent) {
        let key = keyPress.key;
        switch (key) {
            case '0-1': this.calcButtonPress(key); break;
            case '1-1': this.calcButtonPress(key); break;
            case '2-1': this.calcButtonPress(key); break;
            case '3-1': this.calcButtonPress(key); break;
            case '4-1': this.calcButtonPress(key); break;
            case '5-1': this.calcButtonPress(key); break;
            case '6-1': this.calcButtonPress(key); break;
            case '7-1': this.calcButtonPress(key); break;
            case '8-1': this.calcButtonPress(key); break;
            case '9-1': this.calcButtonPress(key); break;
            case '+-1': this.calcButtonPress(key); break;
            case '--1': this.calcButtonPress(key); break;
            case '*-1': this.calcButtonPress(key); break;
            case '/-1': this.calcButtonPress(key); break;
            case '(-1': this.calcButtonPress(key); break;
            case ')-1': this.calcButtonPress(key); break;
            case '.-1': this.calcButtonPress(key); break;
            case 'pi' : this.calcButtonPress(key); break;
            case '=': this.calcButtonPress(key); break;
            case 'Enter': this.calcButtonPress('='); break;
        }
    }

    // Return true if the next input matches something that makes sense
    validateInput(input) {
        let lastValue = this.lcdValue.substr(this.lcdValue.length - 1, 1);
        this.editParenthesisFlag(input, true);
        if (!lastValue || '(/*+.'.indexOf(lastValue) > -1) {
            if (')*/'.indexOf(input) > -1) {
                this.editParenthesisFlag(input, false);
                return false;
            }
            if (lastValue === '.' && ')(-+'.indexOf(input) > -1) {
                this.editParenthesisFlag(input, false);
                return false;
            }
        }
        if (this.numbersPattern.test(lastValue) && this.parenthesisFlag) {
            if (')'.indexOf(input) > -1) {
                this.editParenthesisFlag(input, false);

                return false;
            }
        }
        return true;
    }

    // For every open parenthesis, we must have a closed parenthesis
    // We use this for input validation before we evaluate
    editParenthesisFlag(input: string, add: boolean) {
    }

    // In case of anticipated input error: flash the lcd red
    flashLCD() {
        let currentLCDValue = this.lcdValue;
        this.lcdValue = 'Input Error';
        this.lcdBackGroundColor = '#a02626';
        setTimeout(() => {
            this.lcdBackGroundColor = '#424242';
            //this.lcdBackGroungColor = 'Green';
            this.lcdValue = currentLCDValue;
        }, 500);
    }

    // If the LCD contains an expression -> clear the last entry (backspace)
    // If the LCD contains a result -> clear the entire value
    clearButtonPress() {
            this.lcdValue = '';
            this.expression = '=';
            this.parenthesisFlag = 0;
    }

    // AC = All Clear (remove entire value)
    flipACButton(input: boolean) {
        this.lcdValueExpression = input;
    }

    // Helperfunction used by standardizeString()
    replaceBy(target: string, find: string, replace: string) {
        return target
            .split(find)
            .join(replace);
    }

    // Function evaluates the input expressions
    // RESULTS will be rouded upto 4 after decimal:
    // 0.1 + 0.2 = 0.300000000000004 -> 0.3000
    // 0.1 * 0.2 = 0.020000000000000004 -> 0.0200
    evaluate() {
        let valueToEvaluate: string = this.lcdValue;
        if (this.completePattern.test(valueToEvaluate) && !this.parenthesisFlag) {
            var math: any = require('node_modules/mathjs/dist/math.js');
            this.lcdValue = math.round(math.eval(this.lcdValue), 4);
            //this.lcdValue = math.round(math.eval(this.lcdValue), 4);
            this.expression = 'LAST Operation: ' + valueToEvaluate + ' = ' + this.lcdValue;
        } else {
            this.expression = 'Cannot resolve: ' + this.lcdValue;
            this.lcdValue = "Error";
            this.lcdBackGroundColor = '#a02626';
            this.parenthesisFlag = 0;
        }
        //this.history = valueToEvaluate;

        this.historyList(valueToEvaluate, this.lcdValue);
    }
  onKey(event:any) { // without type info
  }
  // Function shows history of operations
   historyList(inp, opt) {
          this.history += "\n|#|  "+ inp + "=" + opt + " |#|  ";
        }
   }
