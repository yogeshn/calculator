"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//Author: Abhineet Swain
var core_1 = require('@angular/core');
var CalculatorComponent = (function () {
    function CalculatorComponent() {
        this.lcdValue = '';
        this.lcdValueExpression = false;
        this.clearValue = 'AC';
        this.completePattern = new RegExp("[0-9-+*/.()pi]");
        this.numbersPattern = new RegExp('^[0-9]+$');
        this.lcdBackGroundColor = '#424242';
        this.parenthesisFlag = 0;
        this.expression = '=';
        this.history = '';
    }
    // Manages all calculator buttons except "AC", "Reset", & "Clear".
    CalculatorComponent.prototype.calcButtonPress = function (inputValue) {
        if (this.lcdValue === 'Error' || this.lcdValue === 'NaN')
            this.lcdValue = '';
        if (this.validateInput(inputValue)) {
            if ('/*-+'.indexOf(inputValue) === -1 && !this.lcdValueExpression) {
            }
            else if (this.completePattern.test(inputValue)) {
                this.lcdValue += inputValue;
            }
            else if (inputValue === 'pi') {
                this.lcdValue += inputValue;
            }
            else if (inputValue === 'sin') {
                this.lcdValue += inputValue;
            }
            else if (inputValue === 'cos') {
                this.lcdValue += inputValue;
            }
            else if (inputValue === 'tan') {
                this.lcdValue += inputValue;
            }
            else if (inputValue === 'log') {
                this.lcdValue += inputValue;
            }
            else if (inputValue === '=') {
                this.evaluate();
                inputValue = "";
                this.lcdValue += inputValue;
                return;
            }
            this.flipACButton(true);
        }
        else {
            this.flashLCD();
        }
    };
    CalculatorComponent.prototype.random = function () {
        var math = require('node_modules/mathjs/dist/math.js');
        this.lcdValue = math.round(math.random(1, 1000), 2);
    };
    /*****************************************************************
     Little Typescript/Javascript function to RESET TextBox.
     Also refreshed the Page too.
    *****************************************************************/
    CalculatorComponent.prototype.reset = function () {
        this.lcdValue = '';
        this.expression = '';
        location.reload();
    };
    // Function for handling keyboard inputs.
    // Route keyboard input to their appropriate functions.
    CalculatorComponent.prototype.handleKeyboardEvents = function (keyPress) {
        var key = keyPress.key;
        switch (key) {
            case '0-1':
                this.calcButtonPress(key);
                break;
            case '1-1':
                this.calcButtonPress(key);
                break;
            case '2-1':
                this.calcButtonPress(key);
                break;
            case '3-1':
                this.calcButtonPress(key);
                break;
            case '4-1':
                this.calcButtonPress(key);
                break;
            case '5-1':
                this.calcButtonPress(key);
                break;
            case '6-1':
                this.calcButtonPress(key);
                break;
            case '7-1':
                this.calcButtonPress(key);
                break;
            case '8-1':
                this.calcButtonPress(key);
                break;
            case '9-1':
                this.calcButtonPress(key);
                break;
            case '+-1':
                this.calcButtonPress(key);
                break;
            case '--1':
                this.calcButtonPress(key);
                break;
            case '*-1':
                this.calcButtonPress(key);
                break;
            case '/-1':
                this.calcButtonPress(key);
                break;
            case '(-1':
                this.calcButtonPress(key);
                break;
            case ')-1':
                this.calcButtonPress(key);
                break;
            case '.-1':
                this.calcButtonPress(key);
                break;
            case 'pi':
                this.calcButtonPress(key);
                break;
            case '=':
                this.calcButtonPress(key);
                break;
            case 'Enter':
                this.calcButtonPress('=');
                break;
        }
    };
    // Return true if the next input matches something that makes sense
    CalculatorComponent.prototype.validateInput = function (input) {
        var lastValue = this.lcdValue.substr(this.lcdValue.length - 1, 1);
        this.editParenthesisFlag(input, true);
        if (!lastValue || '(/*+.'.indexOf(lastValue) > -1) {
            if (')*/'.indexOf(input) > -1) {
                this.editParenthesisFlag(input, false);
                return false;
            }
            if (lastValue === '.' && ')(-+'.indexOf(input) > -1) {
                this.editParenthesisFlag(input, false);
                return false;
            }
        }
        if (this.numbersPattern.test(lastValue) && this.parenthesisFlag) {
            if (')'.indexOf(input) > -1) {
                this.editParenthesisFlag(input, false);
                return false;
            }
        }
        return true;
    };
    // For every open parenthesis, we must have a closed parenthesis
    // We use this for input validation before we evaluate
    CalculatorComponent.prototype.editParenthesisFlag = function (input, add) {
    };
    // In case of anticipated input error: flash the lcd red
    CalculatorComponent.prototype.flashLCD = function () {
        var _this = this;
        var currentLCDValue = this.lcdValue;
        this.lcdValue = 'Input Error';
        this.lcdBackGroundColor = '#a02626';
        setTimeout(function () {
            _this.lcdBackGroundColor = '#424242';
            //this.lcdBackGroungColor = 'Green';
            _this.lcdValue = currentLCDValue;
        }, 500);
    };
    // If the LCD contains an expression -> clear the last entry (backspace)
    // If the LCD contains a result -> clear the entire value
    CalculatorComponent.prototype.clearButtonPress = function () {
        this.lcdValue = '';
        this.expression = '=';
        this.parenthesisFlag = 0;
    };
    // AC = All Clear (remove entire value)
    CalculatorComponent.prototype.flipACButton = function (input) {
        this.lcdValueExpression = input;
    };
    // Helperfunction used by standardizeString()
    CalculatorComponent.prototype.replaceBy = function (target, find, replace) {
        return target
            .split(find)
            .join(replace);
    };
    // Function evaluates the input expressions
    // RESULTS will be rouded upto 4 after decimal:
    // 0.1 + 0.2 = 0.300000000000004 -> 0.3000
    // 0.1 * 0.2 = 0.020000000000000004 -> 0.0200
    CalculatorComponent.prototype.evaluate = function () {
        var valueToEvaluate = this.lcdValue;
        if (this.completePattern.test(valueToEvaluate) && !this.parenthesisFlag) {
            var math = require('node_modules/mathjs/dist/math.js');
            this.lcdValue = math.round(math.eval(this.lcdValue), 4);
            //this.lcdValue = math.round(math.eval(this.lcdValue), 4);
            this.expression = 'LAST Operation: ' + valueToEvaluate + ' = ' + this.lcdValue;
        }
        else {
            this.expression = 'Cannot resolve: ' + this.lcdValue;
            this.lcdValue = "Error";
            this.lcdBackGroundColor = '#a02626';
            this.parenthesisFlag = 0;
        }
        //this.history = valueToEvaluate;
        this.historyList(valueToEvaluate, this.lcdValue);
    };
    CalculatorComponent.prototype.onKey = function (event) {
    };
    // Function shows history of operations
    CalculatorComponent.prototype.historyList = function (inp, opt) {
        this.history += "\n|#|  " + inp + "=" + opt + " |#|  ";
    };
    CalculatorComponent = __decorate([
        core_1.Component({
            selector: 'js-calculator',
            templateUrl: './app/calculator/calculator.component.html',
            host: {
                '(document:keypress)': 'handleKeyboardEvents($event)'
            }
        }), 
        __metadata('design:paramtypes', [])
    ], CalculatorComponent);
    return CalculatorComponent;
}());
exports.CalculatorComponent = CalculatorComponent;
//# sourceMappingURL=calculator.component.js.map